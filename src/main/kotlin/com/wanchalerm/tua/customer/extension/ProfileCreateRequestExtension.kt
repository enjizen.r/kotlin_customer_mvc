package com.wanchalerm.tua.customer.extension

import com.github.f4b6a3.uuid.UuidCreator
import com.wanchalerm.tua.customer.model.entity.ProfileEntity
import com.wanchalerm.tua.customer.model.entity.ProfilesEmailEntity
import com.wanchalerm.tua.customer.model.entity.ProfilesMobileEntity
import com.wanchalerm.tua.customer.model.entity.ProfilesPasswordEntity
import com.wanchalerm.tua.customer.model.request.ProfileCreateRequest

fun ProfileCreateRequest.buildProfileEntity(
    saltNumber: Int,
    password: String
): ProfileEntity {
    return ProfileEntity().apply {
        code = UuidCreator.getTimeOrderedEpoch().toString()
        firstName = this@buildProfileEntity.firstName
        lastName = this@buildProfileEntity.lastName
        birthDate = this@buildProfileEntity.birthDate
        profilesMobiles = this@buildProfileEntity.buildProfilesMobile(this)
        profilesPasswords = buildProfilesPassword(password, saltNumber, this)
        profilesEmail = this@buildProfileEntity.buildProfilesEmail(this)
    }
}

 fun ProfileCreateRequest.buildProfilesMobile(profileEntity: ProfileEntity) = mutableSetOf(ProfilesMobileEntity(mobileNumber = mobileNumber, profile = profileEntity))

 fun buildProfilesPassword(password: String?, saltNumber: Int?, profileEntity: ProfileEntity) = mutableSetOf(ProfilesPasswordEntity(password = password, saltNumber = saltNumber, profile = profileEntity))

 fun ProfileCreateRequest.buildProfilesEmail(profileEntity: ProfileEntity) = mutableSetOf(ProfilesEmailEntity(email = email, profile = profileEntity))