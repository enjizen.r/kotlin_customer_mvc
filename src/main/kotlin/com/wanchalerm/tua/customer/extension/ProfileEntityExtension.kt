package com.wanchalerm.tua.customer.extension

import com.wanchalerm.tua.common.constant.ResponseEnum
import com.wanchalerm.tua.common.exception.BusinessException
import com.wanchalerm.tua.common.exception.NoContentException
import com.wanchalerm.tua.common.extension.encodeWithSalt
import com.wanchalerm.tua.customer.extension.ProfileEntityExtension.Companion.logger
import com.wanchalerm.tua.customer.model.entity.ProfileEntity
import com.wanchalerm.tua.customer.model.entity.ProfilesPasswordEntity
import com.wanchalerm.tua.customer.model.response.ProfileResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.BeanUtils
import org.springframework.http.HttpStatus


class ProfileEntityExtension {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(this::class.java)
    }
}

fun ProfileEntity.toProfileResponse(): ProfileResponse {
    return ProfileResponse().apply {
        BeanUtils.copyProperties(this@toProfileResponse, this)
        mobileNumber = this@toProfileResponse.profilesMobiles.first { !it.isDeleted }.mobileNumber
        email = this@toProfileResponse.profilesEmail.first { it.isDeleted == false }.email
    }
}

fun ProfileEntity.getPasswordAndSaltNumber(): Pair<ProfilesPasswordEntity, Int> {
    val profilePassword = profilesPasswords.firstOrNull { !it.isDeleted }
        ?: throw NoContentException("profile $code not found password")
    val saltNumber = profilePassword.saltNumber ?: throw NoContentException("profile $code saltNumber not found")
    return profilePassword to saltNumber
}

fun ProfileEntity.authenticateProfile(password: String): String {
    val (profilePassword, saltNumber) = getPasswordAndSaltNumber()

    if (password.encodeWithSalt(saltNumber) != profilePassword.password) throw BusinessException(code = ResponseEnum.CONFLICT.code, "Password does not match")

    if (code.isNullOrBlank()) throw BusinessException(httpStatus = HttpStatus.INTERNAL_SERVER_ERROR, code = ResponseEnum.INTERNAL_SERVER_ERROR.code, message = ResponseEnum.INTERNAL_SERVER_ERROR.message)

    logger.info("Authentication successful for {}", code)
    return code.orEmpty()
}