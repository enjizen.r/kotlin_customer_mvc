package com.wanchalerm.tua.customer.aspect

import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.reflect.MethodSignature
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.util.StopWatch


@Aspect
@Component
class LoggingAspect {
    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Around("execution(* com.wanchalerm.tua.customer.service..*(..)))")
    @Throws(Throwable::class)
    fun profileAllMethods(joinPoint: ProceedingJoinPoint): Any? {
        val stopWatch = StopWatch().also { it.start() } // Start immediately

        try {
            return joinPoint.proceed()
        } finally {
            stopWatch.stop()
            logMethodExecutionTime(joinPoint, stopWatch)
        }
    }

    private fun logMethodExecutionTime(joinPoint: ProceedingJoinPoint, stopWatch: StopWatch) {
        val signature = joinPoint.signature as MethodSignature
        val methodDescription = "${signature.declaringType.simpleName}.${signature.name}"
        logger.info("Execution time of $methodDescription :: ${stopWatch.totalTimeMillis} ms")
    }

}