package com.wanchalerm.tua.customer.controller

import com.wanchalerm.tua.common.constant.ResponseEnum
import com.wanchalerm.tua.common.model.response.ResponseModel
import com.wanchalerm.tua.customer.model.request.ProfileMobileUpdateRequest
import com.wanchalerm.tua.customer.model.response.ProfileResponse
import com.wanchalerm.tua.customer.service.profilemobile.ProfileMobileService
import jakarta.validation.Valid
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1")
class ProfileMobileController (private val profileMobileService: ProfileMobileService) {
    @PatchMapping("/profiles/mobile-number")
    fun updateMobileNumber(@Valid @RequestBody request: ProfileMobileUpdateRequest, @RequestParam("id") id: Int, @RequestParam("code") code: String) : ResponseEntity<ResponseModel<ProfileResponse>> {

        profileMobileService.updateMobileNumber(request.mobileNumber!!, id, code)

        return ResponseEntity
            .status(HttpStatus.OK)
            .body(ResponseModel(ResponseEnum.SUCCESS))
    }
}
