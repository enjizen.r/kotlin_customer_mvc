package com.wanchalerm.tua.customer.controller


import com.wanchalerm.tua.common.constant.ResponseEnum
import com.wanchalerm.tua.common.model.response.ResponseModel
import com.wanchalerm.tua.customer.extension.toProfileResponse
import com.wanchalerm.tua.customer.model.request.ProfileCreateRequest
import com.wanchalerm.tua.customer.model.request.ProfilePasswordRequest
import com.wanchalerm.tua.customer.model.request.ProfileUpdateRequest
import com.wanchalerm.tua.customer.model.response.ProfileResponse
import com.wanchalerm.tua.customer.service.profile.ProfileService
import jakarta.validation.Valid
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/v1")
class ProfileController(private val profileService: ProfileService) {

    @PostMapping("/profiles")
    fun create(@Valid @RequestBody request: ProfileCreateRequest): ResponseEntity<ResponseModel<ProfileResponse>> {
        return ResponseEntity
            .status(HttpStatus.CREATED)
            .body(
                ResponseModel<ProfileResponse>(ResponseEnum.CREATED).setDataObj(profileService.create(request).toProfileResponse())
            )
    }

    @PutMapping("/profiles")
    fun update(
        @Valid @RequestBody request: ProfileUpdateRequest, @RequestParam("id") id: Int,
        @RequestParam("code") code: String
    ): ResponseEntity<ResponseModel<ProfileResponse>> {
        return ResponseEntity
            .status(HttpStatus.OK)
            .body(ResponseModel<ProfileResponse>(ResponseEnum.SUCCESS).setDataObj(profileService.update(request, id, code).toProfileResponse()))
    }

    @GetMapping("/profiles/{code}")
    fun getProfileWithCode(@PathVariable("code") code: String): ResponseEntity<ResponseModel<ProfileResponse>> {
        return ResponseEntity
            .status(HttpStatus.OK)
            .body(ResponseModel<ProfileResponse>(ResponseEnum.SUCCESS).setDataObj(profileService.getByCode(code).toProfileResponse()))
    }

    @DeleteMapping("/profiles")
    fun delete(
        @RequestParam("id") id: Int,
        @RequestParam("code") code: String
    ): ResponseEntity<ResponseModel<Any>> {
        profileService.delete(id, code)
        return ResponseEntity
            .status(HttpStatus.OK)
            .body(ResponseModel(ResponseEnum.SUCCESS))
    }



    @PatchMapping("/profiles/password")
    fun updatePassword(@Valid @RequestBody request: ProfilePasswordRequest, @RequestParam("id") id: Int, @RequestParam("code") code: String) : ResponseEntity<ResponseModel<ProfileResponse>> {
        return ResponseEntity
            .status(HttpStatus.OK)
            .body(ResponseModel<ProfileResponse>(ResponseEnum.SUCCESS).setDataObj(profileService.updatePassword(request.password!!, id, code).toProfileResponse()))
    }
}