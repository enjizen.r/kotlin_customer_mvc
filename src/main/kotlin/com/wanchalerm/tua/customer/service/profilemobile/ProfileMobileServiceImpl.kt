package com.wanchalerm.tua.customer.service.profilemobile

import com.wanchalerm.tua.common.exception.DuplicateException
import com.wanchalerm.tua.common.exception.NoContentException
import com.wanchalerm.tua.customer.model.entity.ProfileEntity
import com.wanchalerm.tua.customer.model.entity.ProfilesMobileEntity
import com.wanchalerm.tua.customer.repository.ProfileMobileRepository
import com.wanchalerm.tua.customer.repository.ProfileRepository
import org.springframework.stereotype.Service

@Service
class ProfileMobileServiceImpl(
    private val profileMobileRepository: ProfileMobileRepository,
    private val profileRepository: ProfileRepository)
    : ProfileMobileService {
    override fun getMobileNumber(mobileNumber: String): ProfilesMobileEntity {
        return profileMobileRepository.findFirstByMobileNumberAndIsDeletedFalse(mobileNumber)
            ?: throw NoContentException(
                message = "Not found mobile number : $mobileNumber"
            )
    }

    override fun updateMobileNumber(mobileNumber: String, profileId: Int, profileCode: String): Boolean {
        validateMobileNumberUniqueness(mobileNumber)
        validateProfileExists(profileId, profileCode)

        return updateProfileMobiles(profileId, mobileNumber)
    }

    private fun validateMobileNumberUniqueness(mobileNumber: String) {
        if (profileMobileRepository.existsByMobileNumber(mobileNumber)) {
            throw DuplicateException("Mobile number $this is duplicate")
        }
    }

    private fun validateProfileExists(profileId: Int, profileCode: String) {
        if (!profileRepository.existsByIdAndCodeAndIsDeletedFalse(profileId, profileCode)) {
            throw NoContentException("Not found profile with id: $profileId and code: $profileCode")
        }
    }

    private fun updateProfileMobiles(profileId: Int, mobileNumber: String): Boolean {
        val profileMobiles = profileMobileRepository.findAllByProfileIdAndIsDeletedFalse(profileId)

        profileMobiles.apply {
             filterNot { it.isDeleted }.forEach { it.isDeleted = true }
            add(ProfilesMobileEntity(mobileNumber = mobileNumber, profile = ProfileEntity(id = profileId)))
        }

        val savedMobiles = profileMobileRepository.saveAll(profileMobiles)
        return savedMobiles.count() == profileMobiles.size
    }
}