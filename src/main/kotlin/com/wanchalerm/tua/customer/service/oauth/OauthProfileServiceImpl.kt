package com.wanchalerm.tua.customer.service.oauth

import com.wanchalerm.tua.common.exception.NoContentException
import com.wanchalerm.tua.customer.extension.authenticateProfile
import com.wanchalerm.tua.customer.service.profileemail.ProfileEmailService
import com.wanchalerm.tua.customer.service.profilemobile.ProfileMobileService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class OauthProfileServiceImpl(
    private val profileEmailService: ProfileEmailService,
    private val profileMobileService: ProfileMobileService
) : OauthProfileService {

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    override fun authenticateWithEmail(email: String, password: String): String {
        logger.info("authenticate with email")
        val profile = profileEmailService.getEmail(email).profile ?: throw NoContentException("$email not found profile")
        return profile.authenticateProfile(password)
    }

    override fun authenticateWithMobileNumber(mobileNumber: String, password: String): String {
        logger.info("authenticate with mobile")
        val profile = profileMobileService.getMobileNumber(mobileNumber).profile ?: throw NoContentException("$mobileNumber not found profile")
        return profile.authenticateProfile(password)
    }

}