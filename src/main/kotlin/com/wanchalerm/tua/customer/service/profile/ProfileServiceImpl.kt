package com.wanchalerm.tua.customer.service.profile

import com.wanchalerm.tua.common.exception.DuplicateException
import com.wanchalerm.tua.common.exception.NoContentException
import com.wanchalerm.tua.common.extension.encodeWithSalt
import com.wanchalerm.tua.common.extension.generateSaltNumber
import com.wanchalerm.tua.common.extension.maskEmail
import com.wanchalerm.tua.common.extension.maskMobileNumber
import com.wanchalerm.tua.customer.extension.buildProfileEntity
import com.wanchalerm.tua.customer.model.entity.ProfileEntity
import com.wanchalerm.tua.customer.model.entity.ProfilesPasswordEntity
import com.wanchalerm.tua.customer.model.request.ProfileCreateRequest
import com.wanchalerm.tua.customer.model.request.ProfileUpdateRequest
import com.wanchalerm.tua.customer.repository.ProfileEmailRepository
import com.wanchalerm.tua.customer.repository.ProfileMobileRepository
import com.wanchalerm.tua.customer.repository.ProfilePasswordRepository
import com.wanchalerm.tua.customer.repository.ProfileRepository
import jakarta.transaction.Transactional
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runBlocking
import org.springframework.beans.BeanUtils
import org.springframework.stereotype.Service


@Service
class ProfileServiceImpl(
    private val profileRepository: ProfileRepository,
    private val profileEmailRepository: ProfileEmailRepository,
    private val profileMobileRepository: ProfileMobileRepository,
    private val profilePasswordRepository: ProfilePasswordRepository
) : ProfileService {


    @Transactional
    override fun create(profileCreateRequest: ProfileCreateRequest): ProfileEntity {

        runBlocking {
             checkDuplicateEmailAndMobileNumber(profileCreateRequest)
        }

        val saltNumber = generateSaltNumber()
        val passwordEncode = profileCreateRequest.password!!.encodeWithSalt(saltNumber)
        return profileRepository.save(profileCreateRequest.buildProfileEntity(saltNumber, passwordEncode))
    }

    override fun update(profileUpdateRequest: ProfileUpdateRequest, id: Int, code: String): ProfileEntity {
        val profileEntity = profileRepository.findByIdAndCodeAndIsDeletedFalse(id, code) ?: throw NoContentException(
            message = "Not found profile with id: $id and code: $code"
        )
        BeanUtils.copyProperties(profileUpdateRequest, profileEntity)
        return profileRepository.save(profileEntity)
    }

    override fun delete(id: Int, code: String) {
        val profileEntity = profileRepository.findByIdAndCodeAndIsDeletedFalse(id, code) ?: throw NoContentException(
            message = "Not found profile with id: $id and code: $code"
        )
        profileEntity.isDeleted = true
        profileRepository.save(profileEntity)
    }

    override fun updatePassword(password: String, id: Int, code: String): ProfileEntity {
        val profileEntity = profileRepository.findByIdAndCodeAndIsDeletedFalse(id, code)
            ?: throw NoContentException("Not found profile with id: $id and code: $code")

        if (profilePasswordRepository.existsByPasswordAndId(password, id)) {
            throw DuplicateException("Password is duplicate")
        }

        profileEntity.profilesPasswords
            .filterNot { it.isDeleted }
            .forEach { it.isDeleted = true }

        val saltNumber = generateSaltNumber()

        val passwordEncode = password.encodeWithSalt(saltNumber)

        profileEntity.profilesPasswords.add(
            ProfilesPasswordEntity(
                password = passwordEncode,
                saltNumber = saltNumber,
                profile = profileEntity
            )
        )

        return profileRepository.save(profileEntity)
    }

    suspend fun checkDuplicateEmailAndMobileNumber(profileCreateRequest: ProfileCreateRequest) = coroutineScope {
        val existsEmail = async { profileEmailRepository.existsByEmail(profileCreateRequest.email!!) }.await()
        val existsMobileNumber =  async { profileMobileRepository.existsByMobileNumber(profileCreateRequest.mobileNumber!!) }.await()

        if (existsEmail) {
            throw DuplicateException(message = "Email ${profileCreateRequest.email!!.maskEmail()} is duplicate")
        }

        if (existsMobileNumber) {
            throw DuplicateException(message = "Mobile number ${profileCreateRequest.mobileNumber!!.maskMobileNumber()} is duplicate")
        }
    }

    override fun getByCode(code: String): ProfileEntity {
        return profileRepository.findByCodeAndIsDeletedFalse(code)
            ?: throw NoContentException(message = "Not found profile with code: $code")
    }
}