package com.wanchalerm.tua.customer

import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication(scanBasePackages = [
    "com.wanchalerm.tua.customer.controller",
    "com.wanchalerm.tua.customer.service",
    "com.wanchalerm.tua.customer.repository",
    "com.wanchalerm.tua.customer.aspect",
    "com.wanchalerm.tua.common.config",
    "com.wanchalerm.tua.common.exception",
    "com.wanchalerm.tua.common.logging",
    "com.wanchalerm.tua.common.logging.filter"])
class TestApplication {
}