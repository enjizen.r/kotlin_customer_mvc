package com.wanchalerm.tua.customer.service.profilemobile

import com.wanchalerm.tua.common.exception.NoContentException
import com.wanchalerm.tua.customer.model.entity.ProfileEntity
import com.wanchalerm.tua.customer.model.entity.ProfilesMobileEntity
import com.wanchalerm.tua.customer.repository.ProfileMobileRepository
import com.wanchalerm.tua.customer.repository.ProfileRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.kotlin.any
import org.mockito.kotlin.eq
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExtendWith(MockitoExtension::class)
class ProfileMobileServiceImplTest {

    @InjectMocks
    private lateinit var subject: ProfileMobileServiceImpl

    @Mock
    private lateinit var profileMobileRepository: ProfileMobileRepository

    @Mock
    private lateinit var profileRepository: ProfileRepository

    @Test
    fun `get mobile number should be success`() {
        val profilesMobileEntity = ProfilesMobileEntity(mobileNumber = "00000222")
        whenever(profileMobileRepository.findFirstByMobileNumberAndIsDeletedFalse(any())).thenReturn(profilesMobileEntity)
        val result = subject.getMobileNumber("00000222")
        Assertions.assertEquals("00000222", result.mobileNumber)
        verify(profileMobileRepository, times(1)).findFirstByMobileNumberAndIsDeletedFalse(any())
    }

    @Test
    fun `get mobile number got null should be error exception`() {
        whenever(profileMobileRepository.findFirstByMobileNumberAndIsDeletedFalse(any())).thenReturn(null)
        assertThrows<NoContentException> {  subject.getMobileNumber("00000222")  }
        verify(profileMobileRepository, times(1)).findFirstByMobileNumberAndIsDeletedFalse(any())
    }

    @Test
    fun `updateMobileNumber should update and return true when data is valid`() {
        val mobileNumber = "1234567890"
        val profileId = 1
        val profileCode = "CODE"
        val profileEntity = ProfileEntity(id = profileId)
        val profileMobiles = mutableListOf(
            ProfilesMobileEntity(mobileNumber = "0987654321", profile = profileEntity)
        )

        whenever(profileMobileRepository.existsByMobileNumber(mobileNumber)).thenReturn(false)
        whenever(profileRepository.existsByIdAndCodeAndIsDeletedFalse(profileId, profileCode)).thenReturn(true)
        whenever(profileMobileRepository.findAllByProfileIdAndIsDeletedFalse(profileId)).thenReturn(profileMobiles)
        whenever(profileMobileRepository.saveAll(profileMobiles)).thenReturn(profileMobiles)

        val result = subject.updateMobileNumber(mobileNumber, profileId, profileCode)

        assertTrue(result)
        verify(profileMobileRepository).existsByMobileNumber(mobileNumber)
        verify(profileRepository).existsByIdAndCodeAndIsDeletedFalse(profileId, profileCode)
        verify(profileMobileRepository).findAllByProfileIdAndIsDeletedFalse(profileId)
        verify(profileMobileRepository).saveAll(eq(profileMobiles))
    }
}
