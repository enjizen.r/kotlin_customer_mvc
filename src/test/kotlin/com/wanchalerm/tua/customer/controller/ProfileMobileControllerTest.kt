package com.wanchalerm.tua.customer.controller

import com.jayway.jsonpath.JsonPath
import com.wanchalerm.tua.customer.TestApplication
import com.wanchalerm.tua.customer.service.profilemobile.ProfileMobileServiceImpl
import org.hamcrest.CoreMatchers.equalTo
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.core.io.ResourceLoader
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


@SpringBootTest(classes = [TestApplication::class])
@AutoConfigureMockMvc
@ActiveProfiles("test")
class ProfileMobileControllerTest {

    @MockBean
    private lateinit var profileMobileService: ProfileMobileServiceImpl

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var resourceLoader: ResourceLoader

    @Test
    fun `update mobile number`() {
        val resource = resourceLoader.classLoader!!.getResourceAsStream("profile_update_mobile_number.json")
        val content = JsonPath.parse(resource).jsonString()

        whenever(profileMobileService.updateMobileNumber(any(), any(), any())).thenReturn(true)

        mockMvc.perform(
            MockMvcRequestBuilders.patch("/v1/profiles/mobile-number")
                .queryParam("id", "1")
                .queryParam("code", "b7be0453-c799-48cb-9ba8-61fce4c203a2")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect { jsonPath("$.status.code", equalTo("2000")) }
            .andExpect { jsonPath("$.status.message", equalTo("Success")) }
        verify(profileMobileService, times(1)).updateMobileNumber(any(), any(), any())
    }
}